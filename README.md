# Flaskdown

Browse markdown files from various folders

_Note: this project is meant to be run locally. It is not secure and should not be exposed to the big bad internet._

## Setup via pipenv

1. `pipenv install` install dependencies.
2. `pipenv shell` activate the virtual environment.
3. `flask run` run the server.

## Dependencies

Python dependencies: Flask and mistune. Bootstrap is added via cdn.
