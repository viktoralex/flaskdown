#!/usr/bin/env python3

import json
from os import listdir
from os.path import isfile, join, normpath, basename

from flask import Flask, render_template, abort
import mistune

app = Flask(__name__, instance_relative_config=True)

app.config.from_object('config') # read /config.py

try:
    app.config.from_pyfile('config.py') # read /instance/config.py
except:
    app.logger.warning('No pyfile configuration found (/instance/config.py)')
    pass # it's fine if it fails


@app.context_processor
def inject_files():
    paths = []
    for i, path in enumerate(app.config['PATHS']):
        foldername = basename(normpath(path))

        files = [
            dict(name=f, path_id=i)
            for f in listdir(path)
            if isfile(join(path, f)) and f.endswith('.md')]
        files.sort(key=lambda f: f['name'])
        files.reverse()

        paths.append(dict(
            foldername=foldername,
            path=path,
            files=files))

    return dict(paths=paths)


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/<int:path_id>/<path:markdown_file>")
def markdown(path_id, markdown_file):
    if path_id < 0 or path_id >= len(app.config['PATHS']):
        abort(404)

    try:
        with open(join(app.config['PATHS'][path_id], markdown_file), 'r') as f:
            markdown = mistune.Markdown()
            markdown_parsed = markdown(f.read())

        return render_template(
            "markdown.html",
            title=markdown_file,
            markdown_parsed=markdown_parsed,
            markdown_file=markdown_file)
    except FileNotFoundError:
        abort(404)

    # Should not happend but if it does, just fail safe
    return render_template("index.html")


@app.errorhandler(404)
def page_not_found(error):
    return render_template("not_found.html", title="Oh no!")
