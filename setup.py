#!/usr/bin/env python

from distutils.core import setup

setup(
    name='Flaskdown',
    version='0.1',
    description='Browse markdown files from various folders',
    author='Viktor Alex Brynjarsson',
    author_email='viktoralex@viktoralex.is',
    url='https://gitlab.com/viktoralex/flaskdown',
    packages=[],
    install_requires=[
        'Flask==1.0.2',
        'mistune==0.8.3',
    ]
)
